<img src="https://i.imgur.com/thGy32h.jpg" align="right" width="200px" />

# xCore (Version: 1.0-Alpha)
<!---
[      ![pipeline status](https://gitlab.com/LIONant/properties/badges/master/pipeline.svg)](https://gitlab.com/LIONant/properties/commits/master)
[            ![Docs](https://img.shields.io/badge/docs-ready-brightgreen.svg)](https://gitlab.com/LIONant/properties/blob/master/docs/Documentation.md)
<br>
[          ![Clang C++17](https://img.shields.io/badge/clang%20C%2B%2B17-compatible-brightgreen.svg)]()
[            ![GCC C++17](https://img.shields.io/badge/gcc%20C%2B%2B17-compatible-brightgreen.svg)]()
--->
[   ![Visual Studio 2019](https://img.shields.io/badge/Visual%20Studio%202019-compatible-brightgreen.svg)]()
<!---
<br>
[            ![Platforms](https://img.shields.io/badge/Platforms-All%20Supported-blue.svg)]()
<br>
--->
[             ![Feedback](https://img.shields.io/badge/feedback-welcome-brightgreen.svg)](https://gitlab.com/LIONant/properties/issues)
[![Contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg)](https://gitlab.com/LIONant/properties)
[              ![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## Low Level Cross Platform Library
<img src="https://i.imgur.com/9a5d2ee.png" align="right" width="150px" />

Simple yet powerful low level API for **C++ 17/20**. It leverages many of the C++ features to provide a simple
to use API to the end user yet deliver near optimum code. The library is focus for real time applications
so:

- No exceptions
- Limit dependencies
- asserts everywhere


## How to build?
Go to the build directory and run: **xcore\builds\UpdateDependencies.bat**
<br>
After it is successfull you should be able to load the example project **xcore\builds\XCORE-Examples.vs2019\XCORE-Examples.sln**.

## What do you need to include in your project?
Please include:
- xcore.h
- xcore.cpp
- xcore_profiler_1.cpp
- xcore_profiler_2.cpp

### Configurations

xCore will want to access its configuration .h file as well as its dependencies configuration files. I have put examples of them at xcore/src/Settings.
However you should copy those files to your own directory and change its contents at will.
- xcore_user_settings.h (The configuration for xCore)
- PropertyConfig.h (The configuration for properties)

If you are compiling for **C++ 20** you will need to compile for debug with **/Zi** to disable the Edit and Continue as tracy is incompatible with it.

## Dependencies

- [Properties](https://gitlab.com/LIONant/properties)
- [Span](https://github.com/tcbrindle/span)
- [Meow Hash](https://github.com/RedSpah/meow_hash_cpp)
- [Tracy](https://github.com/wolfpld/tracy)
- [zstd](http://https://github.com/facebook/zstd)

## Other Notes

ADD: https://github.com/erich666/jgt-code/blob/master/Volume_04/Number_4/Moller1999/fromtorot.c



